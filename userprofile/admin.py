from django.contrib import admin

from userprofile.models import UserProfile


class UserProfileAdmin(admin.ModelAdmin):
    list_display = ('user', 'birth_date', 'hometown',)


admin.site.register(UserProfile, UserProfileAdmin)
