from django.conf.urls import url, include
from rest_framework import routers

from userprofile.views import (UserProfileViewset,
                               UserProfileMemberViewset,)


manage_router = routers.DefaultRouter()
manage_router.register(r'user-profile',
                       UserProfileViewset,
                       base_name='user_profile')

member_router = routers.DefaultRouter()
member_router.register(r'user-profile',
                       UserProfileMemberViewset,
                       base_name='member_user_profile')


urlpatterns = [
    url(r'^manage/', include(manage_router.urls)),
    url(r'^member/', include(member_router.urls)),
]