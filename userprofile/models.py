from django.contrib.auth.models import User
from django.db import models


class UserProfile(models.Model):
    user = models.ForeignKey(User, null=True, blank=True,
                             related_name='user_profile',
                             on_delete=models.SET_NULL)
    birth_date = models.DateField(null=True, blank=True)
    hometown = models.CharField(max_length=100, null=True, blank=True)

    class Meta:
        db_table = 'userprofile_userprofile'
        permissions = (('list_userprofile', 'Can list userprofile'),)

    def __str__(self):
        return f'{self.user.username}'
