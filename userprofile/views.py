from django.shortcuts import render
from rest_framework import mixins, viewsets

from userprofile.models import UserProfile
from userprofile.serializers import (UserProfileSerializer,
                                     UserProfileMemberSerializer,)


class UserProfileViewset(mixins.RetrieveModelMixin,
                         mixins.CreateModelMixin,
                         mixins.ListModelMixin,
                         mixins.UpdateModelMixin,
                         viewsets.GenericViewSet):
    model = UserProfile
    queryset = UserProfile.objects.all().order_by('id')
    permission_classes = []  # TODO
    serializer_class = UserProfileSerializer


class UserProfileMemberViewset(mixins.RetrieveModelMixin,
                               viewsets.GenericViewSet):
    model = UserProfile
    queryset = UserProfile.objects.all().order_by('id')
    permission_classes = []  # TODO
    serializer_class = UserProfileMemberSerializer
